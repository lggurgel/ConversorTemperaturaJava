package usuario;

import java.util.Scanner;

import conversor.*;

public class main {

	public static void main(String[] args) {

		Scanner leitor = new Scanner(System.in);
		int menu;
		
		System.out.println("1-Celsius   2-Kelvin   3-Fahrenheit");
		System.out.print("Temperatura de uso: ");
		menu = leitor.nextInt();
		
		System.out.print("Valor da temperatura: ");
		Double valorTemperatura = Double.parseDouble(leitor.next());
		
		Temperatura temperatura = null;
		
		switch (menu) {
		case 1:
			temperatura = new Celsius(valorTemperatura);
			break;
			
		case 2:
			temperatura = new Kelvin(valorTemperatura);
			break;
			
		case 3:
			temperatura = new Fahrenheit(valorTemperatura);
			break;

		default:
			System.out.println("Valor incorreto.");
			break;
		}
		
		temperatura.converterTemperatura();
		
		System.out.println(temperatura.toString());
		
		leitor.close();

	}

}
