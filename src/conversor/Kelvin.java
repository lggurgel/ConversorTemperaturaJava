package conversor;

public class Kelvin extends Temperatura{

	public Kelvin(Double temperatura){
		temperaturaKelvin = temperatura;
	}
	
	public void converterTemperatura(){
		temperaturaCelsius = temperaturaKelvin - 273;
		temperaturaFahrenheit = ((temperaturaCelsius / 5)* 9)+ 32;
	}
	
	@Override
	public String toString() {
		return "Temperatura Kelvin: "+ temperaturaKelvin +
				"\nTemperatura Celsius: "+ temperaturaCelsius +
				"\nTemperatura Fahrenheit: " + temperaturaFahrenheit;
	}
	
}
