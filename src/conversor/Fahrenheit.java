package conversor;

public class Fahrenheit extends Temperatura{

	public Fahrenheit(Double temperatura) {
		temperaturaFahrenheit = temperatura;
	}
	
	public void converterTemperatura(){
		temperaturaCelsius = ((temperaturaFahrenheit - 32) * 5)/ 9;
		temperaturaKelvin = temperaturaCelsius + 273;
	}

	@Override
	public String toString() {
		return "Temperatura Fahrenheit: " + temperaturaFahrenheit
				+  "\nTemperatura Kelvin: "+ temperaturaKelvin
				+ "\nTemperatura Celsius: "+ temperaturaCelsius;
	}
	
}
