package conversor;

public class Celsius extends Temperatura{

	public Celsius(Double temperatura){
		temperaturaCelsius = temperatura;
	}
	
	public void converterTemperatura(){
		temperaturaKelvin = temperaturaCelsius + 273;
		temperaturaFahrenheit = ((temperaturaCelsius / 5)* 9)+ 32;
	}

	@Override
	public String toString() {
		return "Temperatura Celsius: "+ temperaturaCelsius +
				"\nTemperatura Kelvin: "+ temperaturaKelvin +
				"\nTemperatura Fahrenheit: " + temperaturaFahrenheit;
	}
	
}
