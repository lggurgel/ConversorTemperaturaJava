package conversor;

public abstract class Temperatura {

	protected Double temperaturaCelsius;
	protected Double temperaturaFahrenheit;
	protected Double temperaturaKelvin;
	
	public void converterTemperatura(){
		
	}

	public Double getTemperatura() {
		return temperaturaCelsius;
	}

	public void setTemperatura(Double temperatura) {
		this.temperaturaCelsius = temperatura;
	}

	public Double getTemperaturaFahrenheit() {
		return temperaturaFahrenheit;
	}

	public void setTemperaturaFahrenheit(Double temperaturaFahrenheit) {
		this.temperaturaFahrenheit = temperaturaFahrenheit;
	}

	public Double getTemperaturaKelvin() {
		return temperaturaKelvin;
	}

	public void setTemperaturaKelvin(Double temperaturaKelvin) {
		this.temperaturaKelvin = temperaturaKelvin;
	}
	
	
	
	
	
}
